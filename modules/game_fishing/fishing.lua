
function init()
  connect(g_game, {onGameEnd = _closeEverything})
  local protocolGame = g_game.getProtocolGame()
  ProtocolGame.registerExtendedOpcode(17, _onReceiveExtendedOpcode)
  _createWindows()

  g_keyboard.bindKeyPress('Alt+1', function() g_game.getProtocolGame():sendExtendedOpcode(17, "{action = 'useFishingRod', slot = 1}") local virtualCommandBtn = g_game.getLocalPlayer():getInventoryItem(InventorySlotNeck) modules.game_interface.startUseWith(virtualCommandBtn) end, modules.game_interface.getRootPanel())
  g_keyboard.bindKeyPress('Alt+2', function() g_game.getProtocolGame():sendExtendedOpcode(17, "{action = 'useFishingRod', slot = 2}") local virtualCommandBtn = g_game.getLocalPlayer():getInventoryItem(InventorySlotNeck) modules.game_interface.startUseWith(virtualCommandBtn) end, modules.game_interface.getRootPanel())
  g_keyboard.bindKeyPress('Alt+3', function() g_game.getProtocolGame():sendExtendedOpcode(17, "{action = 'useFishingRod', slot = 3}") local virtualCommandBtn = g_game.getLocalPlayer():getInventoryItem(InventorySlotNeck) modules.game_interface.startUseWith(virtualCommandBtn) end, modules.game_interface.getRootPanel())
end

function terminate()
  disconnect(g_game, {onGameEnd = _closeEverything})
  local protocolGame = g_game.getProtocolGame()
  ProtocolGame.unregisterExtendedOpcode(17)
  _destroyWindows()
end

function _createWindows()
  window = g_ui.displayUI('fishing_window.otui')
end

function _destroyWindows()
  window:destroy()
end

function _onReceiveExtendedOpcode(protocol, opcode, buffer)

  local information = loadstring("return "..buffer)()

  if information.action == "useVirtualCommand" then
    local virtualCommandBtn = g_game.getLocalPlayer():getInventoryItem(InventorySlotNeck)

    if virtualCommandBtn then modules.game_interface.startUseWith(virtualCommandBtn) end

  elseif information.action == "openFishingWindow" then

    window:getChildById("level"):setText("Level\n"..information.info.level)
    window:getChildById("levelprogressbar"):setPercent(information.info.levelPercent)

    window:recursiveGetChildById("fishing"):setText("Fishing: "..information.info.skills.fishing)
    window:recursiveGetChildById("farming"):setText("Farming: "..information.info.skills.farming)
    window:recursiveGetChildById("arqueology"):setText("Arqueology: "..information.info.skills.arqueology)
    window:recursiveGetChildById("breeding"):setText("Breeding: "..information.info.skills.breeding)
    window:recursiveGetChildById("engineering"):setText("Engineering: "..information.info.skills.engineering)

    window:recursiveGetChildById("catches"):setText("Catches: "..information.info.otherInfo.catches)
    window:recursiveGetChildById("pvpwins"):setText("PvP Wins: "..information.info.otherInfo.pvpwins)
    window:recursiveGetChildById("pvploses"):setText("PvP Loses: "..information.info.otherInfo.pvploses)
    window:recursiveGetChildById("tournament"):setText("Tournament: "..information.info.otherInfo.tournament)

    for index, info in pairs(information.rods) do
      local rodChild = window:getChildById("rod"..index)

      if info.removeEnabled then
        rodChild:getChildById("selectButton"):getChildById("removeButton"):show()
        rodChild:getChildById("selectButton"):getChildById("removeButton").onClick = function(self) g_game.getProtocolGame():sendExtendedOpcode(17, "{action = 'removeFishingRod', slot = "..index.."}") self:getParent():getParent():getParent():hide() end
      else
        rodChild:getChildById("selectButton"):getChildById("removeButton"):hide()
        rodChild:getChildById("selectButton"):getChildById("removeButton").onClick = nil
      end

      rodChild:setItemId(info.clientId)
      rodChild:getChildById("selectButton").onClick = function(self) g_game.getProtocolGame():sendExtendedOpcode(17, "{action = 'selectFishingRod', slot = "..index.."}") local virtualCommandBtn = g_game.getLocalPlayer():getInventoryItem(InventorySlotNeck) modules.game_interface.startUseWith(virtualCommandBtn) self:getParent():getParent():hide() end
    end
    window:show()
  end

  return false
end
function hide()
  embWindow:hide()
end

function init()
  connect(g_game, 'onTextMessage', onClick)
  embWindow = g_ui.displayUI('emb')
  embWindow:hide()
end

function terminate()
  disconnect(g_game, 'onTextMessage', onClick)
end

function onClick(mode, text)
  if mode == MessageModes.Failure then
    if text:find('#tower#,') then
      local itemid = tonumber(string.explode(text, ',')[2])
      if tonumber(itemid) == 15714 then
      embWindow:show()
      end
    end
  end
end